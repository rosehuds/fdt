extern crate fdt;

use std::{env::args, fs::File, io::Read};

fn main() {
    let mut file = File::open(args().nth(1).unwrap()).unwrap();
    let mut data = vec![];
    file.read_to_end(&mut data).unwrap();

    let dt = fdt::DeviceTree::new(&data).unwrap();
    let mut node_iter = dt.nodes();
    let mut node_path = String::from("/");
    let mut last_parents = 0i8;
    for node in &mut node_iter {
        let depth_diff = node.parents as i8 - last_parents;
        last_parents = node.parents as i8;

        match depth_diff {
            x if x <= 0 => {
                trim_node_names(&mut node_path, -x + 1);
            }
            1 => {}
            _ => panic!("fdt parser: sudden gain of parents"),
        }
        node_path.push_str(node.name);
        node_path.push('/');
        println!("{}, {}", node_path, node.offset);
    }
}

fn trim_node_names(string: &mut String, n: i8) {
    for _ in 0..n {
        string.pop();
        'inner: loop {
            if string.ends_with('/') || string == "" {
                break 'inner;
            } else {
                string.pop();
            }
        }
    }
}
