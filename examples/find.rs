extern crate fdt;

use std::{fs::File, io::Read};

// Argument 1 is path to dtb, argument 2 is a node path to find, such as `/memory`
fn main() {
    let mut args = std::env::args().skip(1);
    let mut file = File::open(args.next().unwrap()).unwrap();
    let mut data = vec![];
    file.read_to_end(&mut data).unwrap();

    let dt = fdt::DeviceTree::new(&data).unwrap();
    let (node, cells) = dt.find_node(&args.next().unwrap()).unwrap();
    println!("{}: {}", node.name, node.offset);
    println!("address cells: {}", cells.address);
    println!("size cells: {}", cells.size);
    for prop in node.properties() {
        println!("{}", prop.name);
    }
}
