// https://github.com/torvalds/linux/blob/master/Documentation/devicetree/booting-without-of.txt
#![no_std]

extern crate byteorder;

use byteorder::{ByteOrder, BE};

const DT_BEGIN_NODE: u32 = 1;
const DT_END_NODE: u32 = 2;
const DT_PROP: u32 = 3;
const DT_END: u32 = 9;

pub struct DeviceTreeHeader {
    //pub magic: u32,
    pub total_size: u32,
    pub off_dt_struct: u32,
    pub off_dt_strings: u32,
    pub off_mem_rsvmap: u32,

    pub version: u32,
    pub last_comp_version: u32,

    pub boot_cpuid_phys: u32,

    pub size_dt_strings: u32,
    pub size_dt_struct: u32,
}

impl DeviceTreeHeader {
    /// Fails with InvalidMagic if the magic number is not 0xd00dfeed
    pub fn new(raw: &[u8]) -> Result<Self, DeviceTreeError> {
        let magic = BE::read_u32(&raw[0..4]);
        if magic != 0xd00dfeed {
            return Err(DeviceTreeError::InvalidMagic(magic));
        }
        Ok(DeviceTreeHeader {
            total_size: BE::read_u32(&raw[4..8]),
            off_dt_struct: BE::read_u32(&raw[8..12]),
            off_dt_strings: BE::read_u32(&raw[12..16]),
            off_mem_rsvmap: BE::read_u32(&raw[16..20]),

            version: BE::read_u32(&raw[20..24]),
            last_comp_version: BE::read_u32(&raw[24..28]),

            boot_cpuid_phys: BE::read_u32(&raw[28..32]),

            size_dt_strings: BE::read_u32(&raw[32..36]),
            size_dt_struct: BE::read_u32(&raw[36..40]),
        })
    }
}

pub struct DeviceTree<'blob> {
    pub header: DeviceTreeHeader,
    dt_struct: &'blob [u8],
    dt_strings: &'blob [u8],
}

pub struct Node<'blob: 'dt, 'dt> {
    pub name: &'blob str,
    pub parents: u8,
    pub offset: usize,
    dt: &'dt DeviceTree<'blob>,
}

/// A depth-first search for nodes
pub struct NodeIter<'blob: 'dt, 'dt, 'err> {
    pub failed_reason: Option<DeviceTreeError<'err>>,
    offset: usize,
    nodes_deep: u8,
    dt: &'dt DeviceTree<'blob>,
}

pub struct PropIter<'blob: 'dt, 'dt: 'node, 'node, 'err> {
    node: &'node Node<'blob, 'dt>,
    pub failed_reason: Option<DeviceTreeError<'err>>,
    offset: usize,
}

pub struct Property<'blob> {
    pub name: &'blob str,
    pub data: &'blob [u8],
}

#[derive(Debug)]
pub enum DeviceTreeError<'a> {
    InvalidMagic(u32),
    InvalidToken { token: u32, index: usize },
    UnexpectedDtEnd(usize),
    NoRoot,
    NotFound(Option<&'a str>),
    Utf8Error(core::str::Utf8Error),
}

pub struct Cells {
    pub address: u32,
    pub size: u32,
}

impl<'blob> DeviceTree<'blob> {
    pub fn new(dtb: &'blob [u8]) -> Result<Self, DeviceTreeError> {
        let header = DeviceTreeHeader::new(dtb)?;
        let dt_struct = &dtb[header.off_dt_struct as usize
                                 ..header.off_dt_struct as usize + header.size_dt_struct as usize];
        let dt_strings = &dtb[header.off_dt_strings as usize
                                  ..header.off_dt_strings as usize
                                      + header.size_dt_strings as usize];

        if BE::read_u32(&dt_struct[0..4]) != DT_BEGIN_NODE {
            return Err(DeviceTreeError::NoRoot);
        }

        Ok(DeviceTree {
            header,
            dt_struct,
            dt_strings,
        })
    }

    pub fn nodes<'this, 'a>(&'this self) -> NodeIter<'blob, 'this, 'a> {
        NodeIter {
            failed_reason: None,
            offset: 0,
            nodes_deep: 0,
            dt: self,
        }
    }

    /// This fuction finds the first nodes with the given names, *ignoring the `@` and anything after*.
    /// This means that searching for "/memory" may return a node called `/memory@80000000`
    pub fn find_node<'a, 'err>(
        &'a self,
        path: &'err str,
    ) -> Result<(Node<'blob, 'a>, Cells), DeviceTreeError<'err>> {
        let mut current_node: Node<'blob, 'a> = Node {
            name: "",
            parents: 0,
            offset: 0,
            dt: self,
        };
        let mut cells = Cells {
            address: 0,
            size: 0,
        };

        let mut node_iter = self.nodes();

        for node_name in path.split('/') {
            match (&mut node_iter).find(|n| {
                n.name.split('@').next().unwrap() == node_name
                    && (n.parents == current_node.parents + 1
                        || n.name == "")
            }) {
                Some(v) => {
                    for prop in v.properties() {
                        if prop.name == "#address-cells" {
                            cells.address = BE::read_u32(&prop.data[0..4]);
                        } else if prop.name == "#size-cells" {
                            cells.size = BE::read_u32(&prop.data[0..4]);
                        }
                    }
                    current_node = v;
                },
                None => {
                    return Err(DeviceTreeError::NotFound(Some(
                        path.split_at(path.find(node_name).unwrap() + node_name.len()).0,
                    )))
                }
            }
        }

        Ok((current_node, cells))
    }
}

impl<'blob, 'dt> Node<'blob, 'dt> {
    pub fn properties<'node, 'err>(&'node self) -> PropIter<'blob, 'dt, 'node, 'err> {
        let mut props_offset = self.offset + 4 + self.name.len() + 1;
        props_offset += (4 - (props_offset % 4)) % 4;

        PropIter {
            node: self,
            failed_reason: None,
            offset: props_offset,
        }
    }
}

impl<'blob, 'a, 'b> Iterator for NodeIter<'blob, 'a, 'b> {
    type Item = Node<'blob, 'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let dt_struct = self.dt.dt_struct;
        loop {
            let token = BE::read_u32(&dt_struct[self.offset..self.offset + 4]);
            match token {
                DT_BEGIN_NODE => {
                    let name = match str_from_bytes(&dt_struct[self.offset + 4..]) {
                        Ok(v) => v,
                        Err(e) => {
                            self.failed_reason = Some(e);
                            return None;
                        }
                    };
                    let node = Node {
                        name,
                        parents: self.nodes_deep,
                        offset: self.offset,
                        dt: self.dt,
                    };
                    self.offset += 4 + name.len() + 1;
                    self.offset += (4 - (self.offset % 4)) % 4;
                    self.nodes_deep += 1;
                    return Some(node);
                }
                DT_END_NODE => {
                    self.offset += 4;
                    self.nodes_deep -= 1;
                }
                DT_PROP => {
                    self.offset += 4;
                    let prop_data_len = BE::read_u32(&dt_struct[self.offset..self.offset + 4]);
                    self.offset += 8 + prop_data_len as usize;
                    self.offset += (4 - (self.offset % 4)) % 4;
                }
                DT_END => return None,
                _ => {
                    self.failed_reason = Some(DeviceTreeError::InvalidToken {
                        token,
                        index: self.offset,
                    });
                    return None;
                }
            }
        }
    }
}

impl<'blob, 'dt, 'node, 'err> Iterator for PropIter<'blob, 'dt, 'node, 'err> {
    type Item = Property<'blob>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let dt_struct = self.node.dt.dt_struct;
            let dt_strings = self.node.dt.dt_strings;
            let token = BE::read_u32(&dt_struct[self.offset..self.offset + 4]);
            match token {
                DT_PROP => {
                    self.offset += 4;
                    let data_len = BE::read_u32(&dt_struct[self.offset..self.offset + 4]) as usize;
                    self.offset += 4;
                    let name_offset = BE::read_u32(&dt_struct[self.offset..self.offset + 4]) as usize;
                    let name = match str_from_bytes(&dt_strings[name_offset..]) {
                        Ok(v) => v,
                        Err(e) => {
                            self.failed_reason = Some(e);
                            return None;
                        },
                    };
                    self.offset += 4;
                    let data = &dt_struct[self.offset..self.offset + data_len];
                    self.offset += data_len;
                    self.offset += (4 - (self.offset % 4)) % 4;
                    return Some(Property {
                        name,
                        data,
                    });
                },
                //TODO: should we handle intermingled properties and children?
                DT_BEGIN_NODE => return None,
                DT_END_NODE => return None,
                DT_END => {
                    self.failed_reason = Some(DeviceTreeError::UnexpectedDtEnd(self.offset));
                    return None;
                },
                _ => {
                    self.failed_reason = Some(DeviceTreeError::InvalidToken {
                        token,
                        index: self.offset,
                    });
                    return None;
                }
            }
        }
    }
}

fn str_from_bytes<'blob, 'err>(bytes: &'blob [u8]) -> Result<&'blob str, DeviceTreeError<'err>> {
    let mut len = 0;
    while bytes[len] != 0 {
        len += 1;
    }
    core::str::from_utf8(&bytes[0..len]).map_err(DeviceTreeError::Utf8Error)
}
